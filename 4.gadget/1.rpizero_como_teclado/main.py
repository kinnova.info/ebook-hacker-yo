import os
import teclado
from time import sleep

menu =[
    "[0] Salir",
    "[1] Obtener password wifi",
    "[2] Historial navegación incluido incognito",
    "[3] Reparar conexión de red",
    "[4] Renovar IP",
    "[5] Hacer ping",
    "[6] Habilitar el firewall",
    "[7] Desactivar el firewall",
    "[8] Ver redes Wifi guardadas",
    "[9] Exportar perfiles wifi",
    "[10] Obtener valores de todas las NIC"
]

while True:
    #Limpia pantalla
    os.system("clear")
    resp = False
    cod = -1

    # Muestra menu y obtiene opción del usuario
    print(menu[0])
    print(menu[1])
    print(menu[2])
    print(menu[3])
    print(menu[4])
    print(menu[5])
    print(menu[6])
    print(menu[7])
    print(menu[8])
    print(menu[9])
    print(menu[10])
    try:
        cod = int(input("Seleccione una tarea: "))
    except:
        print("ERROR: Entrada no válida")

    os.system("clear")
    # Procesa la elección del usuario
    if not 0 <= cod < len(menu):
        print(f"ERROR: Opción debe estar entre 0 y {len(menu) - 1}")
        input("\nPresione una tecla para continuar...")
        continue

    print("** Ejecutando la tarea **")
    print(menu[cod])
    
    # Ejecuta la acción seleccionada
    if cod == 0:        
        break
    elif cod == 1:
        wifi = input("Nombre de la red wifi : ")
        cmd = f"netsh wlan show profiles name={wifi} key=clear <enter>"
    elif cod == 2:
        cmd = "ipconfig /displaydns <enter>"
    elif cod == 3:
        cmd = """netsh int ip reset & netsh winsock reset & ipconfig /flushdns & netsh int ip delete arpcache & ipconfig /renew <enter>"""
    elif cod == 4:
        cmd = "ipconfig /renew <enter>"
    elif cod == 5:
        ip = input("IP o nombre de equipo : ")
        cmd = f"ping {ip} <enter>"
    elif cod == 6:
        cmd = "netsh advfirewall set allprofiles state on <enter>"
    elif cod == 7:
        cmd = "netsh advfirewall set allprofiles state off <enter>"
    elif cod == 8:
        cmd = "netsh wlan show profiles <enter>"
    elif cod == 9:
        path = input("Especifique ruta : ")
        cmd = f"netsn wlan export profile key=clear folder={path} <enter>"
    elif cod == 10:
        cmd = "ipconfig /all <enter>"

    # Abre la consola cmd
    print("Abriendo consola ...")
    teclado.exec("<left-meta> r")
    sleep(1)
    teclado.exec("cmd <enter>")
    sleep(2)
    print("Ejecutando comando ...")

    # Ejecuta el comando
    resp = teclado.exec(cmd)
    # Comprueba si el comando se ejecutó
    if resp == True:
        print("Comando ejecutado :)")
    else:
        print("ERROR: Comando NO ejecutado:(")

    input("\nPresione una tecla para continuar...")