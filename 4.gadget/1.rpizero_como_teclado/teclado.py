# Códigos de teclas modificadoras
# SHIFT, CONTROL, ALT y GUI
mapeo_teclas_modificadoras = {
    "<left-ctrl>": 0x01,
    "<right-ctrl>": 0x10,
    "<left-shift>": 0x02,
    "<right-shift>": 0x20,
    "<left-alt>": 0x04,
    "<right-alt>": 0x40,
    "<left-meta>": 0x08,
    "<right-meta>": 0x80,
}

# Códigos de las teclas
mapeo_teclas = {
    "a": [0x00, 0x04],
    "b": [0x00, 0x05],
    "c": [0x00, 0x06],
    "d": [0x00, 0x07],
    "e": [0x00, 0x08],
    "f": [0x00, 0x09],
    "g": [0x00, 0x0a],
    "h": [0x00, 0x0b],
    "i": [0x00, 0x0c],
    "j": [0x00, 0x0d],
    "k": [0x00, 0x0e],
    "l": [0x00, 0x0f],
    "m": [0x00, 0x10],
    "n": [0x00, 0x11],
    "o": [0x00, 0x12],
    "p": [0x00, 0x13],
    "q": [0x00, 0x14],
    "r": [0x00, 0x15],
    "s": [0x00, 0x16],
    "t": [0x00, 0x17],
    "u": [0x00, 0x18],
    "v": [0x00, 0x19],
    "w": [0x00, 0x1a],
    "x": [0x00, 0x1b],
    "y": [0x00, 0x1c],
    "z": [0x00, 0x1d],
    "A": [0x02, 0x04],
    "B": [0x02, 0x05],
    "C": [0x02, 0x06],
    "D": [0x02, 0x07],
    "E": [0x02, 0x08],
    "F": [0x02, 0x09],
    "G": [0x02, 0x0a],
    "H": [0x02, 0x0b],
    "I": [0x02, 0x0c],
    "J": [0x02, 0x0d],
    "K": [0x02, 0x0e],
    "L": [0x02, 0x0f],
    "M": [0x02, 0x10],
    "N": [0x02, 0x11],
    "Ñ": [0x00, 0x31],
    "O": [0x02, 0x12],
    "P": [0x02, 0x13],
    "Q": [0x02, 0x14],
    "R": [0x02, 0x15],
    "S": [0x02, 0x16],
    "T": [0x02, 0x17],
    "U": [0x02, 0x18],
    "V": [0x02, 0x19],
    "W": [0x02, 0x1a],
    "X": [0x02, 0x1b],
    "Y": [0x02, 0x1c],
    "Z": [0x02, 0x1d],
    "1": [0x00, 0x1e],
    "2": [0x00, 0x1f],
    "3": [0x00, 0x20],
    "4": [0x00, 0x21],
    "5": [0x00, 0x22],
    "6": [0x00, 0x23],
    "7": [0x00, 0x24],
    "8": [0x00, 0x25],
    "9": [0x00, 0x26],
    "0": [0x00, 0x27],
    "<enter>": [0x00, 0x28],
    "<esc>": [0x00, 0x29],
    "<backspace>": [0x00, 0x2a],
    "<tab>": [0x00, 0x2b],
    " ": [0x00, 0x2c],
    "'": [0x00, 0x2d],
    "=": [0x00, 0x2e],
    "[": [0x00, 0x2f],
    "]": [0x00, 0x30],
    "#": [0x00, 0x32],
    ";": [0x00, 0x33],
    "<grave_accent>": [0x00, 0x35],
    "<comma>": [0x00, 0x36],
    ",": [0x00, 0x36],
    "<dot>": [0x00, 0x37],
    ".": [0x00, 0x37],
    "<slash>": [0x00, 0x38],
    "/": [0x00, 0x38],
    "<caps_lock>": [0x00, 0x39],
    "<f1>": [0x00, 0x3a],
    "<f2>": [0x00, 0x3b],
    "<f3>": [0x00, 0x3c],
    "<f4>": [0x00, 0x3d],
    "<f5>": [0x00, 0x3e],
    "<f6>": [0x00, 0x3f],
    "<f7>": [0x00, 0x40],
    "<f8>": [0x00, 0x41],
    "<f9>": [0x00, 0x42],
    "<f10>": [0x00, 0x43],
    "<f11>": [0x00, 0x44],
    "<f12>": [0x00, 0x45],
    "<print_screen>": [0x00, 0x46],
    "<scroll_lock>": [0x00, 0x47],
    "<pause>": [0x00, 0x48],
    "<insert>": [0x00, 0x49],
    "<home>": [0x00, 0x4a],
    "<page_up>": [0x00, 0x4b],
    "<delete>": [0x00, 0x4c],
    "<end>": [0x00, 0x4d],
    "<page_down>": [0x00, 0x4e],
    "<right_arrow>": [0x00, 0x4f],
    "<left_arrow>": [0x00, 0x50],
    "<down_arrow>": [0x00, 0x51],
    "<up_arrow>": [0x00, 0x52],
    "<num_lock>": [0x00, 0x53],
    "<keypad_slash>": [0x00, 0x54],
    "*": [0x00, 0x55],
    "-": [0x00, 0x56],
    "<keypad_plus>": [0x00, 0x57],
    "+": [0x00, 0x57],
    "<keypad_enter>": [0x00, 0x58],
    "<keypad_1>": [0x00, 0x59],
    "<keypad_2>": [0x00, 0x5a],
    "<keypad_3>": [0x00, 0x5b],
    "<keypad_4>": [0x00, 0x5c],
    "<keypad_5>": [0x00, 0x5d],
    "<keypad_6>": [0x00, 0x5e],
    "<keypad_7>": [0x00, 0x5f],
    "<keypad_8>": [0x00, 0x60],
    "<keypad_9>": [0x00, 0x61],
    "<keypad_0>": [0x00, 0x62],
    "<keypad_dot>": [0x00, 0x63],
    "<keyboard_application>": [0x00, 0x65],
    "<keyboard_power>": [0x00, 0x66],
    "<keypad_equal>": [0x00, 0x67],
    "\\": [0x00, 0x64],
    "#": [0x00, 0x32],
    ",": [0x00, 0x36],
    ".": [0x00, 0x37],
    '!': [0x02, 0x1e],
    '"': [0x02, 0x1f],
    ";": [0x02, 0x36],
    '$': [0x02, 0x21],
    "%": [0x02, 0x22],
    "&": [0x02, 0x23],
    "/": [0x02, 0x24],
    "(": [0x02, 0x25],
    ")": [0x02, 0x26],
    "=": [0x02, 0x27],
    ":": [0x02, 0x37],
    "_": [0x02, 0x38],
    "{": [0x00, 0x34],
    "^": [0x40, 0x34],
    "}": [0x00, 0x31],
    "|": [0x00, 0x35],
    "@": [0x40, 0x1f],
    "`": [0x40, 0x31],
}

modifier = 0x00

def _enviar_tecla(key):
    '''
    Inyecta las teclas hacia el dispositivo
    '''
    try:
        # Abrir el archivo binario de dispositivo HID (/dev/hidg0)
        # En modo lectura/escritura
        with open('/dev/hidg0', 'rb+') as fd:
            # Si la tenga presionada es modificadora
            if key in mapeo_teclas_modificadoras:
                global modifier
                modifier |= mapeo_teclas_modificadoras[key]
            else:
                cod_tecla = mapeo_teclas[key]
                # Inicializa el buffer a ceros
                # usado para enviar el evento de tecla
                buffer = [0] * 8

                # Si la tecla para presionar es Control, Shift, Alt o GUI
                buffer[0] = modifier | cod_tecla[0]

                # Enviar pulsación de tecla
                buffer[2] = cod_tecla[1]
                fd.write(bytes(buffer))

                # Enviar liberación de tecla
                buffer = [0] * 8
                fd.write(bytes(buffer))

                # Se reinician las teclas modificadoras
                modifier = 0x00
        return True
    except OSError as e:
        print("Error al enviar el evento de tecla:", e)
    except KeyError as e:
        print("Error al acceder a la key:", e)

    return False


def exec(command):
    '''
    Ejecuta una instrucción inyectando las teclas
    '''
    resp = False
    command = prepare(command)
    # Divide el command en palabras
    for cmd in command.split("<|>"):
        # Si el comando está mapeado
        # Envia el comando completo
        if cmd in mapeo_teclas or \
                cmd in mapeo_teclas_modificadoras:
            resp = _enviar_tecla(cmd)
        else:
            # Envía el comando letra por letra
            for tecla in cmd:
                resp = _enviar_tecla(tecla)

    return resp


def prepare(command):
    '''
    Prepara el comando para ser inyectado
    '''
    result = []
    for cmd in command.split():
        #Si no es modificadora agrega espacio
        if cmd not in mapeo_teclas_modificadoras:
            cmd += " "
        result.append(cmd)
    
    # Suprime el espacio final y retorna el comando
    return "<|>".join(result).rstrip()
