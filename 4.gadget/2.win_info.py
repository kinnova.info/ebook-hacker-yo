'''
REALIZAR ESTE SCRIPT PERO CON HID GADGET
HACIENDO USO DEL SCRIPT RPIZERO_COMO_TECLADO
PARA LA INYECCIÓN DE TECLAS
'''
import psutil
import subprocess

def informacion_sistema():
    uname = subprocess.run(['uname', '-a'], capture_output=True, text=True)
    print(uname.stdout)

def listar_procesos():
    for process in psutil.process_iter(['pid', 'name', 'username']):
        print(f"PID: {process.info['pid']}, Nombre: {process.info['name']}, Usuario: {process.info['username']}")

def desinstalar_apps_win10():
    # Agrega aquí el comando para desinstalar aplicaciones en Windows 10
    print("Función de desinstalar aplicaciones aún no implementada.")

def mostrar_puertos_abiertos():
    netstat = subprocess.run(['netstat', '-ano'], capture_output=True, text=True)
    print(netstat.stdout)

def mostrar_estado_firewall():
    firewall = subprocess.run(['netsh', 'advfirewall', 'show', 'allprofiles', 'state'], capture_output=True, text=True)
    print(firewall.stdout)

def main():
    while True:
        print("\nMenú:")
        print("1. Información del sistema")
        print("2. Listar Procesos")
        print("3. Desinstalar apps por defecto Win10")
        print("4. Mostrar puertos abiertos")
        print("5. Mostrar el estado del firewall")
        print("0. Salir")

        opcion = input("Seleccione una opción: ")

        if opcion == '1':
            informacion_sistema()
        elif opcion == '2':
            listar_procesos()
        elif opcion == '3':
            desinstalar_apps_win10()
        elif opcion == '4':
            mostrar_puertos_abiertos()
        elif opcion == '5':
            mostrar_estado_firewall()
        elif opcion == '0':
            break
        else:
            print("Opción inválida. Por favor, seleccione una opción válida.")

if __name__ == "__main__":
    main()