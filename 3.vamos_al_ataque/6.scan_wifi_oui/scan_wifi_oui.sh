#!/bin/bash

echo "Escaneando redes WiFi..."

# Usa el comando iw para escanear las redes WiFi 
# y el comando grep para obtener los datos requeridos
wifi_info=$(sudo iw dev wlan0 scan | 
	    grep -E "BSS [[:xdigit:]]{2}(:[[:xdigit:]]{2}){5}|SSID:|signal:|freq:")

# Inicializar las variables
mac=""
freq=""
signal=""
ssid=""

# Imprimir el encabezado de la tabla
printf "%-18s %-7s %-7s %-28s %s\n" "MAC" "freq" "signal" "SSID" "Fabricante"

# Recorrer las líneas y obtener los datos de cada registro
while read -r line; do
  if [[ "$line" =~ BSS\ ([[:xdigit:]]{2}(:[[:xdigit:]]{2}){5}) ]]; then
    mac="${BASH_REMATCH[1]}"
    # Eliminar los dos puntos (:) de la MAC
    mac_prefix="${mac//:}"
    # Obtener solo los primeros 6 caracteres (tres primeros bloques) de la MAC
    mac_prefix="${mac_prefix:0:6}"
    # Obtener el nombre del fabricante a partir de la MAC
    fabricante=$(grep -i "^$mac_prefix" oui.txt | awk -F '\t' '{print $3}')
  elif [[ "$line" == freq:* ]]; then
    freq=$(echo "$line" | awk '{print $2}')
  elif [[ "$line" == signal:* ]]; then
    signal=$(echo "$line" | awk '{print $2}')
  elif [[ "$line" == SSID:* ]]; then
    ssid=$(echo "$line" | awk '{printf substr($0,7)}')
    # Imprimir la fila de la tabla
    printf "%-18s %-7s %-7s %-28s %s\n" "$mac" "$freq" "$signal" "$ssid" "$fabricante"
  fi
done <<< "$wifi_info"
