#!/bin/bash

#Hace un barrido ping a las IPs en el rango 1-20 de la red 192.168.1
for i in {1..20}; do
    ping -W 1 -c 1 192.168.1.$i | grep ttl
done
