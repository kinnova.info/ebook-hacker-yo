#!/bin/bash

#Barrido ping para determinar los equipos activos y su posible sistema operativo
for i in {1..30}; do
    ip="192.168.0.$i"
    res_ping=$(ping -W 0.5 -c 1 $ip | grep ttl)
    if [ ! -z "$res_ping" ]; then
        case $res_ping in
        *64*)
            echo "$ip :: UP :: Linux"
            ;;
        *128*)
            echo "$ip :: UP :: Windows"
            ;;
        *)
            echo "$ip :: UP :: Otro"
            ;;
        esac
    fi
done
