import subprocess as sub
import re
import math

mac_target = input("[·] Dirección MAC a buscar: ").upper()
wifi_list = set(input("[·] Wi-Fi guardadas en el equipo. Ej: cafe,miwifi: ").upper().split(","))

if mac_target or wifi_list:
    tcpdump_args = ('tcpdump', '-e', '-s0', '-i', 'mon0', '-vvv')
    process = sub.Popen(tcpdump_args, stdout=sub.PIPE)

    mac_regex = re.compile(r'(?:[0-9a-fA-F]:?){12}')  # regex para la MAC

    for row in iter(process.stdout.readline, ''):
        packet = row.decode(encoding="utf-8").upper()

        # Limpia la cadena
        packet = packet.replace("SA:", "").replace("DA:", "").replace("BSSID:", "").replace("RA:", "").replace("TA:", "")

        # Obtiene las direcciones MAC que vienen en el paquete
        mac_founded = re.findall(mac_regex, packet)

        if mac_target in mac_founded:
            print(packet)

        for wifi in wifi_list:
            if wifi and wifi in packet:
                wifi_info = re.findall(r"(?:REQUEST|BEACON)\s\((.*?)\)\s", packet)
                wifi_name, signal = wifi_info[0] if wifi_info else ("", "")
                signal_float = float(signal[:-3])

                print(f"Objetivo {mac_target} [- ENCONTRADO -] :)")
                print(f"Nombre Wifi: {wifi_name}")
                print(f"Signal: {signal}")
                print()
                break
else:
    print("Error: Debe especificar la MAC o alguna Wi-Fi")