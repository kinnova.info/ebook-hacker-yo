#!/bin/bash

# Pedir al usuario la dirección MAC que se quiere detectar
echo "Ingrese dirección MAC a detectar (formato xx:xx:xx:xx:xx:xx):"
read mac_address

# Crear una tarjeta de red virtual en modo monitor
sudo iw phy phy0 interface add mon0 type monitor

# Activar la tarjeta de red virtual
sudo ip link set mon0 up

# Variable para mostrar la notificación emergente solo una vez
notification_displayed=false

# Ejecutar tcpdump indefinidamente para capturar el tráfico de red
sudo tcpdump -i mon0 -e -v | while read line; do
    # Comprobar si se ha capturado algún paquete de la dirección MAC especificada
    if echo $line | grep -qi $mac_address; then
    	echo "Equipo con MAC $mac_address detectado."
    	# Reproduce un sonido de notificación
    	paplay /usr/share/sounds/freedesktop/stereo/bell.oga        
        #Solo muestra la notificación emergente una vez
        if [ "$notification_displayed" = false ]; then
        	# Muestra una notificación emergente
       		notify-send -t 10000 "Equipo encontrado" "Equipo con MAC $mac_address detectado."
       		notification_displayed=true
        fi
    fi
done

# Eliminar la tarjeta de red virtual mon0
sudo ip link set mon0 down
sudo iw dev mon0 del
