#!/bin/bash

echo "Escaneando redes WiFi..."

# Usa el comando iw para escanear las redes WiFi 
# y el comando grep para obtener los datos requeridos, incluyendo el canal (channel)
wifi_info=$(sudo iw dev wlan0 scan | 
            grep -E "BSS [[:xdigit:]]{2}(:[[:xdigit:]]{2}){5}|SSID:|signal:|freq:|DS Parameter set: channel")

# Inicializar las variables
mac=""
freq=""
signal=""
ssid=""
channel=""

# Imprimir el encabezado de la tabla, incluyendo la columna "Channel"
printf "%-20s %-10s %-10s %-10s %s\n" "MAC" "freq" "signal" "Channel" "SSID"

# Recorrer las líneas y obtener los datos de cada registro
while read -r line; do
  if [[ "$line" =~ BSS\ ([[:xdigit:]]{2}(:[[:xdigit:]]{2}){5}) ]]; then
    mac="${BASH_REMATCH[1]}"
  elif [[ "$line" == freq:* ]]; then
    freq=$(echo "$line" | awk '{print $2}')
  elif [[ "$line" == signal:* ]]; then
    signal=$(echo "$line" | awk '{print $2}')
  elif [[ "$line" == "DS Parameter set: channel "* ]]; then
    channel=$(echo "$line" | awk '{print $5}')
  elif [[ "$line" == SSID:* ]]; then
    ssid=$(echo "$line" | awk '{printf substr($0,7)}')
    # Imprimir la fila de la tabla, incluyendo el valor del canal
    printf "%-20s %-10s %-10s %-10s %s\n" "$mac" "$freq" "$signal" "$channel" "$ssid"
  fi
done <<< "$wifi_info"
