#!/bin/bash

zoom=16z

# Verificar si el archivo existe
if [ -f "location.txt" ]; then
    # Leer el contenido del archivo y decodificarlo a JSON usando jq
    resp=$(cat location.txt)
    lat=$(echo "$resp" | jq -r .lat)
    lon=$(echo "$resp" | jq -r .lon)
else
    # Si el archivo no existe o el contenido no es válido JSON, establecer latitud y longitud en 0
    lat=0
    lon=0
fi

# Función para realizar el escaneo de redes WiFi
scan_wifi() {
    echo "Escaneando redes WiFi..."

    # Usa el comando iw para escanear las redes WiFi
    # y el comando grep para obtener los datos requeridos (BSS y SSID)
    wifi_info=$(sudo iw dev wlan0 scan |
        grep -E "BSS [[:xdigit:]]{2}(:[[:xdigit:]]{2}){5}|SSID:")

    # Inicializar las variables
    mac=""
    ssid=""
    url="https://www.google.com/maps/@$lat,$lon,$zoom"

    # Imprimir el encabezado de la tabla
    printf "%-20s %-30s %s\n" "MAC" "SSID" "Maps"

    # Recorrer las líneas y obtener los datos de cada registro
    while read -r line; do
        if [[ "$line" =~ BSS\ ([[:xdigit:]]{2}(:[[:xdigit:]]{2}){5}) ]]; then
            mac="${BASH_REMATCH[1]}"
        elif [[ "$line" == SSID:* ]]; then
            ssid=$(echo "$line" | awk '{printf substr($0,7)}')
            # Imprimir la fila de la tabla
            printf "%-20s %-30s %s\n" "$mac" "$ssid" "$url"
        fi
    done <<<"$wifi_info"
}

# Mostrar los datos del escaneo en pantalla y guardarlos en el archivo de texto
scan_wifi | tee -a redes.txt
