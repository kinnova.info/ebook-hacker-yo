<?php
// Obtener el valor del parámetro "lon" enviado a través de la solicitud GET
$lon = $_GET["lon"];
// Obtener el valor del parámetro "lat" enviado a través de la solicitud GET
$lat = $_GET["lat"];
// Crear un array asociativo con las coordenadas obtenidas del cliente
$gps = ["lon" => $lon, "lat" => $lat];
// Convertir el array en una cadena JSON y guardarla en el archivo "location.txt"
file_put_contents('location.txt', json_encode($gps));
// Imprimir "ok" en la respuesta HTTP para indicar que la operación fue exitosa
echo "ok";
