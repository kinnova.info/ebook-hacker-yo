# Script de Monitoreo de Memoria USB
# ----------------------------------
# Este script se ejecuta continuamente hasta cuando se conecta una memoria USB con la etiqueta 'MEMORIA_USB'.
# Una vez que se detecta, almacena la letra de unidad en la variable $driveLetter.

$driveLetter = $null  # Inicializamos la variable $driveLetter como nula.

# Bucle de detección de la memoria USB
while ($null -eq $driveLetter) {
    # Utilizamos Get-WmiObject para buscar una instancia de Win32_Volume con la etiqueta 'MEMORIA_USB'.
    $driveInfo = Get-WmiObject -Query "SELECT * FROM Win32_Volume WHERE Label='MEMORIA_USB'"
    
    if ($driveInfo) {
        # Si se encuentra una unidad con la etiqueta 'MEMORIA_USB', almacena su letra de unidad en $driveLetter.
        $driveLetter = $driveInfo.DriveLetter
    } else {
        # Si no se encuentra la unidad, espera 5 segundos antes de volver a intentarlo.
        Start-Sleep -Seconds 5
    }
}

# Verifica si el archivo mostrar_contrasenas_wifi.ps1 existe en la unidad de la memoria USB.
if (Test-Path -Path "$driveLetter\mostrar_contrasenas_wifi.ps1") {
    Invoke-Expression -Command "powershell.exe -ExecutionPolicy Bypass -File $driveLetter\mostrar_contrasenas_wifi.ps1"
}