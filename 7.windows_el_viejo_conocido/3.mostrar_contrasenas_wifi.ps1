$wifiProfiles = netsh wlan show profiles | Select-String "Perfil de todos los usuarios"

foreach ($profile in $wifiProfiles) {
    $ssid = $profile -replace "Perfil de todos los usuarios\s+:\s+", ""
    $password = (netsh wlan show profile name=$ssid key=clear) -match "Contenido de la clave\s+:\s+(\S+)\s*"

    if ($password) {
        Write-Host "SSID: $ssid"
        Write-Host "Contraseña: $($Matches[1])"
        Write-Host "---------------------"
    }
}