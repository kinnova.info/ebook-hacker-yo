/**
 * Busca en la diferentes sitios web
 * @param {string} site Nombre del sitio de busqueda
 * @returns 
 */
function search(site) {
    //Objetivo a buscar
    let target = document.getElementById('target').value
    if (target.length > 0) {
        switch (site) {
            case "phone":
                createWindow(`https://whocalld.com/${target}`);
                break;
            case "email":
                createWindow(`https://emailrep.io/${target}`);
                break;
            case "ip":
                createWindow(`https://ipleak.net/${target}`);
                break;
            case "image":
                //createWindow(`https://www.facebook.com/public/${target}`);
                break;
            case "facebook":
                createWindow(`https://www.facebook.com/public/${target}`);
                break;
            case "instagram":
                createWindow(`https://www.google.com/search?q=site:instagram.com ${target}`);
                break;
            case "tiktok":
                createWindow(`https://www.tiktok.com/search/user?q=${target}`);
                break;
            case "twitter":
                createWindow(`https://twitter.com/search?q=${target}&f=user`);
                break;
            case "linkedin":
                createWindow(`https://www.google.com/search?q=site:linkedin.com ${target}`);
                break;
            case "google":
                createWindow(`https://www.google.com/search?q=${target}`);
                break;
            case "duckduckgo":
                createWindow(`https://duckduckgo.com/?q=${target}`);
                break;
            case "bing":
                createWindow(`https://bing.com/?q=${target}`);
                break;
            case "drive":
                createWindow(`https://www.google.com/search?q=site:drive.google.com ${target}`);
                break;
            case "pdf":
                createWindow(`https://www.google.com/search?q=ext:pdf ${target}`);
                break;
            case "pinterest":
                createWindow(`https://www.pinterest.es/search/pins/?q=${target}`);
                break;
            case "flickr":
                createWindow(`https://www.flickr.com/search/people/?username=${target}`);
                break;
            default:
                break;
        }
    } else {
        alert("Escriba el perfil a buscar")
    }
}

/**
 * Crea ventana modal con los resultados de busqueda
 * @param {string} url URL del sitio de busqueda
 * @param {string} title Titulo de la ventana
 * @returns 
 */
function createWindow(url) {
    //Crea una ventana modal
    window.open(url,'_blank');
}