<?php
// Definir una función para obtener la dirección IP del cliente
function obtenerDireccionIP() {
    // Comprobar si la variable 'HTTP_CLIENT_IP' contiene un valor no vacío
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        // Si es así, asignar el valor a la variable $ip
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        // Si 'HTTP_X_FORWARDED_FOR' contiene un valor no vacío, asignarlo a la variable $ip
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        // De lo contrario, asignar la dirección IP del cliente al valor de la variable $ip
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    // Devolver la dirección IP obtenida
    return $ip;
}

// Llamar a la función para obtener la dirección IP del cliente y asignar el resultado a la variable $ip
$ip = obtenerDireccionIP();

// Obtener el User Agent del cliente y asignarlo a la variable $userAgent
$userAgent = $_SERVER['HTTP_USER_AGENT'];

// Obtener la fecha y hora actual en formato 'Y-m-d H:i:s' y asignarla a la variable $fecha
$fecha = date('Y-m-d H:i:s');

// Crear un array asociativo con la información recopilada (dirección IP, User Agent y fecha)
$datos = [
    'ip' => $ip,
    'user_agent' => $userAgent,
    'fecha' => $fecha
];

// Convertir el array $datos en una cadena JSON y agregar el carácter de nueva línea (PHP_EOL) al final
$jsonDatos = json_encode($datos) . PHP_EOL;

// Escribir la cadena JSON en el archivo 'visita.log' 
// en modo Append de añadir al final del archivo (FILE_APPEND)
file_put_contents('visita.log', $jsonDatos, FILE_APPEND);
?>