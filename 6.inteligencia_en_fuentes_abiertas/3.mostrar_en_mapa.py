import httpx
import webbrowser

def localizar_en_mapa_por_ip():
    print("Mostrar en el mapa a un objetivo a través de su IP")
    ip = input('Ingrese la dirección IP a localizar: ')
    url = 'https://ipinfo.io/' + ip
    response = httpx.get(url).json()

    print("Información sobre la dirección IP:")
    print(f"IP: {response['ip']}")
    print(f"Ubicación: {response['loc']}")
    print(f"País: {response['country']}")
    print(f"Ciudad: {response['city']}")
    print(f"ISP: {response['org']}")

    resp = input('¿Desea abrir el mapa para visualizar la ubicación? [s/n]: ')
    if resp.lower() == 's':
        # Abre Google Maps con la ubicación proporcionada por la API
        url_mapa = f"https://www.google.com/maps/search/?api=1&query={response['loc']}"
        webbrowser.open(url_mapa)

if __name__ == "__main__":
    localizar_en_mapa_por_ip()