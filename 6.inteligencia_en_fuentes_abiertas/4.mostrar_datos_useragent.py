# Debe instalar librería
# pip install user-agents
from user_agents import parse

def obtener_datos_desde_user_agent(user_agent_str):
    user_agent = parse(user_agent_str)
    datos = {
        "Tipo de dispositivo": user_agent.device.family,
        "Marca del dispositivo": user_agent.device.brand,
        "Modelo del dispositivo": user_agent.device.model,
        "Sistema operativo": user_agent.os.family,
        "Versión del sistema operativo": user_agent.os.version_string,
        "Navegador": user_agent.browser.family,
        "Versión del navegador": user_agent.browser.version_string,
        "Es bot o crawler": user_agent.is_bot,
    }
    return datos

user_agent_str = input("Ingrese el User Agent: ")
datos = obtener_datos_desde_user_agent(user_agent_str)
print("Datos obtenidos del User Agent:")
for key, value in datos.items():
    print(f"{key}: {value}")