# COLOCAR OTROS COMANDOS QUE OBTENGAS DATOS RELEVANTES

# Lista de apps instaladas
pm list packages 

# para ver el operador de telefonía 
logcat -b radio | grep -i sim    

#Get Device Serial Number
adb get-serialno

#Mostrar mensajes SMS
adb shell content query --uri content://sms/ --projection _id:address:date:body

# Obtener todos los contactos
adb shell content query --uri content://contacts/phones/ --projection display_name:number

#Take Screenshots
adb shell screencap -p "/path/to/screenshot.png"

# IMEI
adb shell service call iphonesubinfo 1

#Obtener el número de telefono asociado al whatsapp. No requiere root
adb shell dumpsys telecom | grep whatsapp.net