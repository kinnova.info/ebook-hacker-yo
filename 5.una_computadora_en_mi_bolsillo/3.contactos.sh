#!/bin/bash

# Verifica que adb esté instalado
if ! command -v adb &> /dev/null; then
    echo "ADB no está instalado en el sistema. Asegúrate de tenerlo instalado."
    exit 1
fi

# Función para verificar y establecer la conexión con el dispositivo Android
check_and_connect_device() {
    if ! adb devices | grep "device$" &> /dev/null; then
        echo "No se ha encontrado ningún dispositivo Android conectado. Conectando..."
        adb start-server
        adb devices | grep "device$" &> /dev/null
        if [ $? -ne 0 ]; then
            echo "No se pudo conectar al dispositivo Android. Asegúrate de que el dispositivo esté conectado y configurado correctamente."
            exit 1
        fi
        echo "Conexión exitosa al dispositivo Android."
    fi
}

# Verificar y conectar al dispositivo Android
check_and_connect_device

# Función para mostrar el menú principal
show_menu() {
    clear
    echo "Menú Principal"
    echo "1. Ver lista de contactos"
    echo "2. Buscar contacto"
    echo "3. Exportar lista de contactos"
    echo "4. Salir"
}

# Función para mostrar la lista de contactos
list_contacts() {
    clear
    echo "Lista de contactos:"
    adb shell content query --uri content://com.android.contacts/data/phones --projection display_name:number
    echo
    read -p "Presiona Enter para volver al menú principal..."
}

# Función para buscar un contacto
search_contact() {
    clear
    read -p "Ingrese el nombre o número de teléfono del contacto a buscar: " query
    adb shell content query --uri content://com.android.contacts/data/phones --projection display_name:number --where "display_name LIKE '%$query%' OR number LIKE '%$query%'"
    echo
    read -p "Presiona Enter para volver al menú principal..."
}

# Función para exportar la lista de contactos a un archivo
export_contacts() {
    clear
    adb shell content query --uri content://com.android.contacts/data/phones --projection display_name:number > lista_contactos.txt
    echo "Lista de contactos exportada a lista_contactos.txt"
    echo
    read -p "Presiona Enter para volver al menú principal..."
}

# Ciclo infinito
while true; do
    show_menu
    read -p "Selecciona una opción: " option
    case $option in
        1)
            list_contacts
            ;;
        2)
            search_contact
            ;;
        3)
            export_contacts
            ;;
        4)
            echo "¡Adiós!"
            exit 0
            ;;
        *)
            echo "Opción no válida. Introduce un número del 1 al 4."
            ;;
    esac
done