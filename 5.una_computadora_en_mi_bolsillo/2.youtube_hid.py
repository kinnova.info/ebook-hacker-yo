
'''
MODIFICAR PARA USB HID GADGET Y LOS ATAJOS DE TECLADO
Y ABRA EL NAVEGADOR Y REPRODUZCA UN VIDEO
'''
import time
import keyboard

def press_key(key):
    keyboard.press_and_release(key)
    time.sleep(0.2)

def open_youtube_and_play(video_query):
    press_key('win')  # Presiona la tecla de Windows (inicio)
    time.sleep(1)

    keyboard.write('youtube.com')  # Escribe "youtube.com" en el buscador
    press_key('enter')  # Presiona la tecla Enter
    time.sleep(2)

    # Buscar y reproducir el video
    keyboard.write(video_query)
    press_key('enter')  # Presiona la tecla Enter
    time.sleep(2)

if __name__ == "__main__":
    video_to_play = "nombre_del_video"  # Reemplaza "nombre_del_video" con el título del video que deseas buscar y reproducir
    open_youtube_and_play(video_to_play)
