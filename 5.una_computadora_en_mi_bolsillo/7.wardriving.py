import os
import json
import sqlite3
import time

start = time.time()
end = start + 5

# Crear la base de datos y la tabla si no existen
with sqlite3.connect("wifi_network.db") as con:
    cur = con.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS wifi_ap (
                    mac TEXT,
                    ssid TEXT,
                    rssi INTEGER,
                    lat REAL,
                    lon REAL
                )''')
    con.commit()

bssid_saved = []

try:
    while True:
        dif_time = time.time() - start
        if dif_time >= 5:
            # Obtener información de geolocalización (GPS)
            stream = os.popen('termux-location')
            r = stream.read()
            capGPS = json.loads(r)
            if capGPS:
                geoloc = capGPS

            start = time.time()

        # Escanear información de redes Wi-Fi
        stream = os.popen('termux-wifi-scaninfo')
        r = stream.read()
        wifi_info = json.loads(r)

        for ap in wifi_info:
            # Para evitar guardar AP ya guardados en el escaneo actual
            if ap['bssid'] not in bssid_saved:
                # Guardar en la base de datos SQLite
                with sqlite3.connect("wifi_network.db") as con:
                    try:
                        cur = con.cursor()
                        cur.execute("INSERT INTO wifi_ap (mac, ssid, rssi, lat, lon) VALUES (?,?,?,?,?)",
                                    (ap['bssid'], ap['ssid'], int(ap['rssi']), float(geoloc["latitude"]), float(geoloc["longitude"])))
                        con.commit()
                        print("Guardado en la base de datos.")
                    except:
                        con.rollback()

                bssid_saved.append(ap['bssid'])
                print('AP: ' + ap['bssid'])
                print(f'Ubicación: {geoloc["latitude"]}, {geoloc["longitude"]}')
                print()

        end = time.time()

except KeyboardInterrupt:
    print('Script interrumpido por el usuario.')