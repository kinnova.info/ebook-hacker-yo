import os, json
from datetime import datetime

wifi_all = []
bssid_saved = []

try:
    while (1):
        stream = os.popen('termux-wifi-scaninfo')
        r = stream.read()
        wifi_info = json.loads(r)
        for ap in wifi_info:
            if not ap['bssid'] in bssid_saved:
                bssid_saved.append(ap['bssid'])
                wifi_all.append(ap)
                print('AP :' + ap['bssid'])
except:
   print('Script stop!')
finally:
   name = 'wifiscan.txt'
   with open(name, 'w') as file:
       file.write(json.dumps(wifi_all))

print('Proceso finalizado')