#!/bin/bash

# VERIFICAR QUE ESTE CONECTADO A TRAVES DE ADB
# MODIFICAR PARA QUE LOS TOQUES COINCIDAN CON LAS UBICACIONES DE LOS ELEMENTOS

# Abre la aplicación de configuración de WiFi en el dispositivo Android
adb shell am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings

# Espera un momento para asegurarte de que la aplicación se abra correctamente
sleep 2

# Realiza un toque en la opción de configuración de redes guardadas
adb shell input tap X Y

# Espera un momento para asegurarte de que la opción se seleccione correctamente
sleep 2

# Realiza un toque en la red WiFi guardada para generar el código QR
adb shell input tap X Y

# Espera un momento para asegurarte de que se seleccione la red WiFi correctamente
sleep 2

# Captura la pantalla para obtener la imagen del código QR
adb shell screencap -p /sdcard/wifi_qr.png

# Copia la imagen del código QR al directorio actual
adb pull /sdcard/wifi_qr.png .

# Cierra la aplicación de configuración de WiFi
adb shell am force-stop com.android.settings