import utils
import menu
from colorama import Fore
import commands

connected = False

def main():
        utils.clear_screen()
        while True:
            try:
                status = f"{Fore.GREEN}connected{Fore.RESET}" if connected else f"{Fore.RED}disconnected{Fore.RESET}"
                cmd = input(f"{Fore.GREEN} droid-control ({status}{Fore.GREEN}) > {Fore.RESET}").strip().lower()
                if cmd in menu.cmd_remote:
                    print("Control remoto")
                elif cmd in menu.cmd_connect:
                    ip_port = input(f"{Fore.LIGHTBLACK_EX} {utils.ARROW} connect (ip:port) > {Fore.RESET}")
                    # Add default port (5555)
                    ip_port = ip_port if ":" in ip_port else f"{ip_port}:5555"
                    if utils.validate_ip_port(ip_port):
                        #Prepare, execute and print command
                        task = f"adb connect {ip_port}"
                        out = commands.exec(task.split(" "))
                        if out["returncode"] == 0:
                            print(out["msg"])
                    else:
                        print(f"{Fore.RED} Error: IP not valid {Fore.RESET}" )
                elif cmd in menu.cmd_wifi:
                    #Repeat until put correct input
                    while True:
                        c = input(f"{Fore.LIGHTBLACK_EX} {utils.ARROW} wifi (on/off) > {Fore.RESET}").strip().lower()
                        task = None
                        if c == "on":
                            task = "adb shell svc wifi enable"                            
                        elif c == "off":
                            task = "adb shell svc wifi disable"
                        elif c in menu.cmd_exit:
                            break
                        else:
                            print(f"{Fore.YELLOW} Enable -> on. Disable -> off. Exit -> e {Fore.RESET}" )
                        
                        if task != None:
                            out = commands.exec(task.split(" "))
                            if out["returncode"] == 0:
                                print(out["msg"])

                elif cmd in menu.cmd_clear:
                    utils.clear_screen()
                elif cmd in menu.cmd_help:
                    helpme()
                elif cmd in menu.cmd_exit:
                    break
                else:
                    print(f"{Fore.RED} Error: command not found {Fore.RESET}" )
                    helpme()
            except KeyboardInterrupt:
                print("Bye!")
                exit(0)
            except Exception as ex:
                print(ex)

def helpme():
    for m in menu.menu_help:
        print(f"\t{Fore.YELLOW} {m[0]:25} {m[1]}")


if __name__ == "__main__":
    main()
