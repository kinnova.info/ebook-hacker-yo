from os import system
import re

#IP:PORT
ip_port_regex = r"^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}):(\d{1,5})$"
ARROW = "  '~>"

def clear_screen():
    system("clear")

def validate_ip_port(ip_port):
    if re.match(ip_port_regex, ip_port):
        return True
    else:
        return False