from subprocess import check_output, CalledProcessError


def exec(cmd):
    """ 
        Prepare, execute and return command output
    """
    try:
        # cmd is a list and not empty
        if isinstance(cmd, list) and len(cmd) > 0:
            msg = check_output(cmd).decode('utf-8')
            returncode = 0
    except CalledProcessError as e:
        msg = e.output
        returncode = 1

    return {
        "returncode": returncode,
        "msg": msg
    }
