cmd_help = ["help", "h"]
cmd_exit = ["exit", "e"]
cmd_list_app = ["list-app", "la"]
cmd_install_app = ["install-app", "ia"]
cmd_uninstall_app = ["uninstall-app", "ua"]
cmd_remote = ["remote", "r"]
cmd_connect = ["connect", "c"]
cmd_take_screen = ["print-screen", "ps"]
cmd_record_screen = ["record-screen", "rs"]
cmd_battery_status = ["battery-status", "bs"]
cmd_shutdown = ["shutdown", "s"]
cmd_reboot = ["reboot", "re"]
cmd_volume_call = ["volume-call", "vc"]
cmd_volume_system = ["volume-system", "vs"]
cmd_volume_notification = ["volume-notification", "vn"]
cmd_system_info = ["system-info", "si"]
cmd_mobile_data = ["imei", "i"]
cmd_wifi = ["wifi", "w"]
cmd_airplane = ["airplane", "a"]
cmd_bluetooth = ["bluetooth", "b"]
cmd_tethering = ["tethering", "t"]
cmd_clear = ["clear", "cs"]


menu_help = [
    ["COMMAND",  "DESCRIPTION"],
    [", ".join(cmd_help),  "Print help"],
    [", ".join(cmd_list_app), "List app installed"],
    [", ".join(cmd_install_app), "Install app"],
    [", ".join(cmd_uninstall_app), "Uninstall a app"],
    [", ".join(cmd_take_screen), "Take print screen"],
    [", ".join(cmd_record_screen), "Record screen"],
    [", ".join(cmd_wifi), "Enable/disable wifi"],
    [", ".join(cmd_clear), "Clear screen"],
    [", ".join(cmd_exit), "Exit"]
]