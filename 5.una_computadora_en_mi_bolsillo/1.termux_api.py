import subprocess
import json

def run_termux_command(command):
    result = subprocess.run([command], capture_output=True, text=True)
    try:
        return json.loads(result.stdout)
    except json.JSONDecodeError:
        return None

def get_battery_info():
    return run_termux_command('termux-battery-status')

def get_ip_address():
    return run_termux_command('termux-telephony-deviceinfo')

def get_wifi_info():
    return run_termux_command('termux-wifi-connectioninfo')

def get_location_info():
    return run_termux_command('termux-location')

if __name__ == "__main__":
    battery_info = get_battery_info()
    ip_info = get_ip_address()
    wifi_info = get_wifi_info()
    location_info = get_location_info()

    if battery_info:
        print("Información de la batería:")
        print(f"Nivel de batería: {battery_info.get('percentage')}%")
        print(f"Estado de carga: {battery_info.get('status')}")
        print(f"Temperatura de la batería: {battery_info.get('temperature', 'N/A')}°C")
    
    if ip_info:
        print("\nInformación de la red:")
        print(f"Dirección IP: {ip_info.get('data_activity', 'N/A')}")

    if wifi_info:
        print("\nInformación de la conexión WiFi:")
        print(f"SSID: {wifi_info.get('ssid', 'N/A')}")
        print(f"MAC Address: {wifi_info.get('mac_address', 'N/A')}")

    if location_info:
        print("\nInformación de la ubicación:")
        print(f"Latitud: {location_info.get('latitude', 'N/A')}")
        print(f"Longitud: {location_info.get('longitude', 'N/A')}")