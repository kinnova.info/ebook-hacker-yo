edad = 16

if edad >= 18:
    print("Puedes votar. ¡Ejerce tu derecho democrático!")
else:
    print("Lo siento, no puedes votar. Debes tener al menos 18 años")
