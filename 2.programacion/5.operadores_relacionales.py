# Igualdad: ==
print(3 == 3) # Salida: True

# Desigualdad !=
print(4 != 3) # Salida: True

# Mayor que: >
print(5 > 2) # Salida: True

# Menor: <
print(3 < 6) # Salida: True

# Mayor o igual que: >=
print(4 >= 4) # Salida: True

# Menor o igual que: <=
print(2 <= 3) # Salida: True