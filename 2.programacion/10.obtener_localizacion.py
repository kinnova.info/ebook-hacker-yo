# Importa librería necesaria para hacer una petición a una página
import httpx

def main():
    direccion_ip = input("Dirección IP [Ej: 1.1.1.1] => ")
    # URL de la página a consultar la IP
    url = f"http://ip-api.com/json/{direccion_ip}"
    # Obtiene la respuesta desde de la página
    respuesta = httpx.get(url)

    # Si no hubo error (200)
    if respuesta.status_code == 200:
        # Obtiene los datos que envió la página
        datos = respuesta.json()
        # Guarda los datos que necesitamos en variables
        pais = datos["country"]
        latitud = datos["lat"]
        longitud = datos["lon"]
        # Imprime por pantalla los datos
        print(f"País: {pais}")
        print(f"Latitud: {latitud}")
        print(f"Longitud: {longitud}")
    else:
        print("No se pudo obtener información para la dirección IP.")

main()
