# Declaración de variables de diferentes tipos
numero_entero = 10
numero_real = 3.14
texto = "Hola mundo"
es_verdadero = True

# Mostrar los valores de las variables
print("Número entero:", numero_entero)
print("Número real:", numero_real)
print("Texto:", texto)
print("Es verdadero?", es_verdadero)
