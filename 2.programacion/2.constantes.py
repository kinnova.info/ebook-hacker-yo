# Declaración de constante
PI = 3.14159

# Cálculo utilizando la constante
radio = 7.0
area = PI * radio * radio

#Mostrar el resultado del cálculo
print("El área del círuclo es:", area)