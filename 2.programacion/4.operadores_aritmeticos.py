# Suma
x = 3+5
print(x)  # Salida: 8

# Resta
x = 7-2
print(x)  # Salida: 5

# Multiplicación
x = 4*3
print(x)  # Salida: 12

# División real
x = 10 / 2
print(x)  # Salida: 5.0

# División Entera
x = 10 // 3
print(x)  # Salida: 3

# Módulo
x = 10 % 3
print(x)  # Salida: 1

